# SublPackagesMirror

A python script which download all sublime packages for offline usages.
Basically generate a mirror of all the servers which serves subime packages. 

## Prerequisites

* Python 3.6

## Usage

* Download all the packages using `packages_downloader.py` (see -h for options)
* Put the output directory behind a HTTP server (nginx, apache or even python http.server module)
* Browse to your server at 'index.html'

Done! from here on it's just like the you are online!

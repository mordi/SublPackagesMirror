''' Script for mirroring Sublime Package Control server. '''
import argparse
import multiprocessing
import os
import re
import shutil
import tempfile
import zipfile

from pathlib import Path
from urllib import request


TEMP_PROTOCOL_MAGIC = '###TEMP://'
INSTALLATION_PAGE_TEMPLATE = 'installation_page_template.html'
INSTALLATION_PAGE_MAGIC = '###SERVER_FULL_PATH###'
MAIN_CHANNEL_FILE = 'channel_v3.json'
PACKAGE_CONTROL_URL = 'https://packagecontrol.io/'
PACKAGE_CONTROL_PKG = 'Package Control.sublime-package'
PACKAGE_CONTROL_SETTINGS = 'Package Control.sublime-settings'
PACKAGE_CONTROL_PKG_URL = PACKAGE_CONTROL_URL + PACKAGE_CONTROL_PKG
DEFAULT_CHANNEL = PACKAGE_CONTROL_URL + MAIN_CHANNEL_FILE


class SublimeMirrorManager:
    def __init__(self, output_dir, server_full_url):
        self.output_dir = output_dir
        self.server_full_url = server_full_url

    def _create_base_out_dir(self):
        ''' Create out dir and put installation page stuff in it. '''
        os.makedirs(self.output_dir, exist_ok=True)
        out_page = Path(self.output_dir, "index.html")
        shutil.copy(INSTALLATION_PAGE_TEMPLATE, out_page)
        text = out_page.read_text()
        out_page.write_text(text.replace(INSTALLATION_PAGE_MAGIC, self.server_full_url))

    def _remove_from_zip(self, zip_name, file_name):
        ''' remove_from_zip '''
        tempdir = tempfile.mkdtemp()
        try:
            tempname = os.path.join(tempdir, 'new.zip')
            with zipfile.ZipFile(zip_name, 'r') as zipread:
                with zipfile.ZipFile(tempname, 'w') as zipwrite:
                    for item in zipread.infolist():
                        if item.filename != file_name:
                            data = zipread.read(item.filename)
                            zipwrite.writestr(item, data)
            shutil.move(tempname, zip_name)
        finally:
            shutil.rmtree(tempdir)

    def _create_custom_package_control(self, dest_file):
        ''' Download and modify package control package '''
        request.urlretrieve(PACKAGE_CONTROL_PKG_URL, dest_file)
        with zipfile.ZipFile(dest_file) as pkg_zip:
            with pkg_zip.open(PACKAGE_CONTROL_SETTINGS) as conf_file:
                conf = conf_file.read().decode()
            conf = conf.replace(PACKAGE_CONTROL_URL, self.server_full_url)
        self._remove_from_zip(dest_file, PACKAGE_CONTROL_SETTINGS)
        with zipfile.ZipFile(dest_file, 'a') as pkg_zip:
            pkg_zip.writestr(PACKAGE_CONTROL_SETTINGS, conf.encode())

    def _get_custom_channel_json(self):
        ''' Download and modify the default channel json '''
        channel_json = request.urlopen(DEFAULT_CHANNEL).read().decode()
        Path(self.output_dir, MAIN_CHANNEL_FILE).write_bytes(
            channel_json.replace('http://', TEMP_PROTOCOL_MAGIC)
                        .replace('https://', TEMP_PROTOCOL_MAGIC)
                        .replace(TEMP_PROTOCOL_MAGIC, self.server_full_url).encode())
        return channel_json

    def _download_file(self, link):
        ''' download_file '''
        try:
            path = os.path.join(self.output_dir, *link.split('//')[1].split('/'))
            os.makedirs(os.path.dirname(path), exist_ok=True)
            request.urlretrieve(link, path)
            # print('Finish!!', link)
        except Exception as exception:
            # print(f'Error at {link}\nException was: {exception}')
            pass

    def _get_links_from_json(self, json_channel):
        ''' get_links_from_json '''
        for match in re.finditer('"https?://.*?"', json_channel):
            yield match.group()[1:-1]

    def _download_all_packages(self, json_channel):
        ''' download_all_packages '''
        with multiprocessing.Pool() as pool:
            pool.map(self._download_file, list(self._get_links_from_json(json_channel))[:100])

    def start(self):
        self._create_base_out_dir()
        self._create_custom_package_control(Path(self.output_dir, PACKAGE_CONTROL_PKG))
        json_channel = self._get_custom_channel_json()
        self._download_all_packages(json_channel)


def main():
    ''' main '''
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument(
        '-s', '--server-full-url', default='localhost:8080/',
        help='The full URL of the server which the packages will be served from.')
    parser.add_argument(
        '-o', '--output-dir', default='output',
        help='The output directory where the packages will be downloaded to.')
    args = parser.parse_args()

    mirror = SublimeMirrorManager(args.output_dir, args.server_full_url)
    mirror.start()


if __name__ == '__main__':
    main()
